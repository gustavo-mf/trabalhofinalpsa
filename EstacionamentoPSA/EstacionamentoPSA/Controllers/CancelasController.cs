﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EstacionamentoPSA;
using EstacionamentoPSA.Models;
using EstacionamentoPSA.Facade;

namespace EstacionamentoPSA.Views
{
    public class CancelasController : Controller
    {
        private CancelaFacadeImpl cancelaFacade = new CancelaFacadeImpl();

        // GET: Cancelas
        public ActionResult Index()
        {
            return View(cancelaFacade.List());
        }

        //custom method
        public ActionResult ValidateTicket(int ticketId)
        {
            string message = cancelaFacade.ValidateTicket(ticketId);
            return new JsonResult { Data = message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        
         // GET: Cancelas/Details/5
         public ActionResult Details(int? id)
         {
             if (id == null)
             {
                 return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
             }
            Cancela cancela = cancelaFacade.Find((int)id);
            if (cancela == null)
             {
                 return HttpNotFound();
             }
             return View(cancela);
         }
        /*
         // GET: Cancelas/Create
         public ActionResult Create()
         {
             return View();
         }

         // POST: Cancelas/Create
         // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
         // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
         [HttpPost]
         [ValidateAntiForgeryToken]
         public ActionResult Create([Bind(Include = "id,nome,tipo,liberada,estacionamento_id")] Cancela cancela)
         {
             //facade.create(Cancela ca);
             if (ModelState.IsValid)
             {
                 db.Cancelas.Add(cancela);
                 db.SaveChanges();
                 return RedirectToAction("Index");
             }

             return View(cancela);
         }
         */
        // GET: Cancelas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cancela cancela = cancelaFacade.Find((int)id);
            if (cancela == null)
            {
                return HttpNotFound();
            }
            return View(cancela);
        }
        
        // POST: Cancelas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,nome,tipo,liberada,estacionamento_id")] Cancela cancela)
        {
            if (ModelState.IsValid)
            {
                cancelaFacade.Edit(cancela);
                return RedirectToAction("Index");
            }
            return View(cancela);
        }
        /*
        // GET: Cancelas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cancela cancela = db.Cancelas.Find(id);
            if (cancela == null)
            {
                return HttpNotFound();
            }
            return View(cancela);
        }

        // POST: Cancelas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Cancela cancela = db.Cancelas.Find(id);
            db.Cancelas.Remove(cancela);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
       
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        } */
    }
}

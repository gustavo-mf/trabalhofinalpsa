﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EstacionamentoPSA;
using EstacionamentoPSA.Models;
using EstacionamentoPSA.Facade;

namespace EstacionamentoPSA.Views
{
    public class TicketsController : Controller
    {
        //private ApplicationDbContext db = new ApplicationDbContext();
        private TicketFacadeImpl ticketFacade = new TicketFacadeImpl();

        // GET: Tickets
        public ActionResult Index()
        {
            return View();
        }

        // GET pay
        public bool Pay(int id)
        {
            return ticketFacade.pay((int)id);
        }

        // GET: Tickets/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = ticketFacade.findTicket((int)id);
            if (ticket == null)
            {
                return HttpNotFound();
            }
            ticket.valor = ticketFacade.calcTicketPrice(ticket);
            return View(ticket);
        }

        // GET: Tickets/Create
        public ActionResult Create()
        {
            return View(ticketFacade.createTicket());
        }
                
        // POST: Tickets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /*[HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Ticket ticket)
        {
            
            ViewBag.Message = ticketFacade.createTicket();
            
            return RedirectToAction("Index");
         
            return View(ticket);
        }*/

        // GET: Tickets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = ticketFacade.findTicket((int)id);
            if (ticket == null)
            {
                return HttpNotFound();
            }
            return View(ticket);
        }

        // POST: Tickets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /*[HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,data,codigo,status,valor,motivo_liberacao,cancela_id")] Ticket ticket)
        {
            if (ModelState.IsValid)
            {
                ticketFacade.edi
                return RedirectToAction("Index");
            }
            return View(ticket);
        }*/

        // GET: Tickets/Delete/5
        /*public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = ticketFacade.findTicket((int)id);
            if (ticket == null)
            {
                return HttpNotFound();
            }
            return View(ticket);
        }

        // POST: Tickets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ticket ticket = ticketFacade.findTicket((int)id);
            db.Tickets.Remove(ticket);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }*/
    }
}

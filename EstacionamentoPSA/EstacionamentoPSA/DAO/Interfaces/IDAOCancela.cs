﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstacionamentoPSA.DAO.Interfaces
{
    interface IDAOCancela
    {
        void Edit(Cancela cancela);
        List<Cancela> List();
        void SaveChanges();
        Cancela Find(int cancelaId);
    }
}

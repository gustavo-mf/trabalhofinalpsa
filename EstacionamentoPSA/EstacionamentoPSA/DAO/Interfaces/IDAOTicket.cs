﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstacionamentoPSA.DAO.Interfaces
{
    interface IDAOTicket
    {
        Ticket create(Ticket ticket);
        List<Ticket> List();
        void Edit(Ticket ticket);
        Ticket details(int ticketid);
        void SaveChanges();        
    }
}

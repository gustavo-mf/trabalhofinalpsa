﻿using EstacionamentoPSA.DAO.Interfaces;
using EstacionamentoPSA.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EstacionamentoPSA.DAO.Implementations
{
    public class DAOCancelaImpl : IDAOCancela
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public void Edit(Cancela cancela)
        {
            db.Entry(cancela).State = EntityState.Modified;
            SaveChanges();
        }

        public Cancela Find(int cancelaId)
        {
            Cancela cancela = db.Cancelas.Find(cancelaId);
            return cancela;
        }

        //esperando a facade do ticket
        public Ticket FindTicket(int ticketId)
        {
            return db.Tickets.Find(ticketId);
        }

        public List<Cancela> List()
        {
            return db.Cancelas.ToList();
        }

        public void SaveChanges()
        {
            db.SaveChanges();
        }
    }
}
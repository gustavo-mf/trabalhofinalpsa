﻿using EstacionamentoPSA.DAO.Interfaces;
using EstacionamentoPSA.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EstacionamentoPSA.DAO.Implementations
{
    public class DAOTicketImpl : IDAOTicket
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public Ticket create(Ticket ticket)
        {
            db.Tickets.Add(ticket);
            db.SaveChanges();
            return ticket;
        }

        public Ticket Find(int ticketCodigo)
        {
            Ticket ticket = db.Tickets.Find(ticketCodigo);
            return ticket;
        }

        public Ticket details(int ticketid)
        {
            Ticket ticket = db.Tickets.Find(ticketid);
            return ticket;
        }

        public void Edit(Ticket ticket)
        {
            db.Entry(ticket).State = EntityState.Modified;
            SaveChanges();
        }

        public List<Ticket> List()
        {
            return db.Tickets.ToList();
        }

        public void SaveChanges()
        {
            db.SaveChanges();
        }

        
    }
}
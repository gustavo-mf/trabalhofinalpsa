﻿using EstacionamentoPSA.DAO.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EstacionamentoPSA.Facade
{
    public class CancelaFacadeImpl : ICancelaFacade
    {
        private DAOCancelaImpl cancelaDAO = new DAOCancelaImpl();
        private DAOTicketImpl ticketDAO = new DAOTicketImpl();

        public Ticket CreateTicket()
        {
            Ticket ticket = new Ticket();
            ticket.data = DateTime.Today;
            ticket.valor = 0;
            ticket.status = 0;
            ticket.codigo = 0;
            ticket.motivo_liberacao = "";
            ticket.cancela_id = 1;

            return ticketDAO.create(ticket);
        }

        public void Edit(Cancela cancela)
        {
            cancelaDAO.Edit(cancela);
        }

        public Cancela Find(int cancelaId)
        {
            return cancelaDAO.Find(cancelaId);
        }

        public List<Cancela> List()
        {
            return cancelaDAO.List();
        }

        //chamar facade do ticker - esperando
        public string ValidateTicket(int ticketId)
        {
            Cancela canc = Find(2); //busca cancela de saida
            if (!canc.liberada)
            {
                Ticket ticket = cancelaDAO.FindTicket(ticketId);
                if (ticket == null)
                {
                    return "Ticket não encontrado";
                }
                if (ticket.status == 1 || ticket.status == 2)
                {
                    //editar status do ticket - esperar o facade do ticket
                    return "Ticket Liberado";
                }
                else
                {
                    return "Ticket Não liberado";
                }
            }else
            {
                return "Cancela Liberada";
            }

           
        }
    }
}
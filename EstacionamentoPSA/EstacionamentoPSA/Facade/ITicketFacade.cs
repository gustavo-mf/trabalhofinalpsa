﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EstacionamentoPSA.Facade
{
    interface ITicketFacade
    {
        Ticket createTicket();
        Boolean releaseTicket(int ticketid, String reason);
        int calcTicketPrice(Ticket ticket);
        Ticket findTicket(int ticketid);
        Boolean pay(int ticketid);
        Boolean release(int ticketid);
        double totalReceived(int day, int month);
        int countPaidTickets(int day, int month);
        int countNonPaidTickets(int day, int month);        
    }
}
﻿using EstacionamentoPSA.DAO.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EstacionamentoPSA.Facade
{
    public class TicketFacadeImpl : ITicketFacade
    {
        private DAOTicketImpl ticketDAO = new DAOTicketImpl();

        public int calcTicketPrice(Ticket ticket)
        {
            /*15 minutos de cortesia.Se o ticket for utilizado para liberação da cancela dentro do período de 15 minutos após a entrada, 
             * o sistema deve liberar a saída, sem a necessidade de efetuar a validaçãovia pagamento.
             * 
            Até 3 horas(inclusive) de permanência: o valor é fixo, sendo atualmente de R$ 5,00.

            Acima de 3 horas, e que não seja pernoite: o valor é fixo, sendo atualmente de R$ 20,00.

            Pernoite(saída após as 8:00 do dia seguinte, sendo que o estacionamento é fechado às 2:00 da manhã): o valor é de R$ 50,00 por 
            pernoite.

            Extravio do ticket: o valor é fixo, sendo atualmente de R$ 50,00.Este caso implica na emissão do ticket com código especial de 
            identificação do motivo.*/

            int valor = -1;
            DateTime today = DateTime.Now;
            TimeSpan ts = today - ticket.data;
            int days = ts.Days;
            int hours = ts.Hours;
            int minutes = ts.Minutes;
            System.Diagnostics.Debug.WriteLine(today);
            System.Diagnostics.Debug.WriteLine(ticket.data);
            System.Diagnostics.Debug.WriteLine(ts);
            System.Diagnostics.Debug.WriteLine(days);
            System.Diagnostics.Debug.WriteLine(hours);
            System.Diagnostics.Debug.WriteLine(minutes);
            //pernoite
            if (days > 0)
            {
                valor = 50;
            }
            else if (hours > 3 || (hours == 3 && minutes > 1))
            {
                valor = 20;
            }
            else if (hours > 0)
            {
                valor = 5;
            }
            else if (minutes > 15)
            {
                valor = 5;
            }
            else if (minutes <= 15)
            {
                valor = 0;
            }

            return valor;
        }

        public int countNonPaidTickets(int day, int month)
        {
            throw new NotImplementedException();
        }

        public int countPaidTickets(int day, int month)
        {
            throw new NotImplementedException();
        }
        

        public Ticket createTicket()
        {
            Ticket ticket = new Ticket();
            ticket.data = DateTime.Now;
            System.Diagnostics.Debug.WriteLine(DateTime.Today);
            ticket.valor = 0;
            ticket.status = 0;
            ticket.codigo = 0;
            ticket.motivo_liberacao = "";
            ticket.cancela_id = 1;
            
            return ticketDAO.create(ticket);
        }

        public Ticket findTicket(int ticketid)
        {
            return ticketDAO.details(ticketid);
        }

        public bool pay(int ticketid)
        {
            Ticket ticket = findTicket(ticketid);
            if (ticket == null)
            {
                return false;
            }
            ticket.status = 1;
            ticket.motivo_liberacao = "pago pelo usuario";
            ticketDAO.SaveChanges();

            return true;
        }

        public bool release(int ticketid)
        {
            throw new NotImplementedException();
        }

        public bool releaseTicket(int ticketid, string reason)
        {
            throw new NotImplementedException();
        }

        public double totalReceived(int day, int month) 
        {
            throw new NotImplementedException();
        }
    }
}
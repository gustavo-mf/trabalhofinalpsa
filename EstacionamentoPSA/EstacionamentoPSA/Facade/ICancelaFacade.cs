﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstacionamentoPSA.Facade
{
    interface ICancelaFacade
    {
        Ticket CreateTicket();
        string ValidateTicket(int ticketId);
        void Edit(Cancela cancela);
        List<Cancela> List();
        Cancela Find(int cancelaId);
    }
}

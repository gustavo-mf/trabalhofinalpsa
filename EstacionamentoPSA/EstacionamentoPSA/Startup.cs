﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EstacionamentoPSA.Startup))]
namespace EstacionamentoPSA
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2017-06-20 00:40:43.52

-- tables
-- Table: Cancelas
CREATE TABLE Cancelas (
    id int  NOT NULL IDENTITY,
    nome varchar(100)  NOT NULL,
    tipo int  NOT NULL,
    liberada bit  NOT NULL,
    estacionamento_id int  NOT NULL,
    CONSTRAINT Cancelas_pk PRIMARY KEY  (id)
);

-- Table: Estacionamentos
CREATE TABLE Estacionamentos (
    id int  NOT NULL IDENTITY,
    vagas int  NOT NULL,
    CONSTRAINT Estacionamentos_pk PRIMARY KEY  (id)
);

-- Table: Tickets
CREATE TABLE Tickets (
    id int  NOT NULL IDENTITY,
    data datetime  NOT NULL,
    codigo int  NOT NULL,
    status int  NOT NULL,
    valor int  NOT NULL,
    motivo_liberacao varchar(100)  NOT NULL,
    cancela_id int  NOT NULL,
    CONSTRAINT Tickets_pk PRIMARY KEY  (id)
);

-- foreign keys
-- Reference: Cancelas_Estacionamentos (table: Cancelas)
ALTER TABLE Cancelas ADD CONSTRAINT Cancelas_Estacionamentos
    FOREIGN KEY (estacionamento_id)
    REFERENCES Estacionamentos (id);

-- Reference: Tickets_Cancelas (table: Tickets)
ALTER TABLE Tickets ADD CONSTRAINT Tickets_Cancelas
    FOREIGN KEY (cancela_id)
    REFERENCES Cancelas (id);

-- End of file.

